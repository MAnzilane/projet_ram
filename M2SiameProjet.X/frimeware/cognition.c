/*
 * File:   cognition.c
 * Author: dehuit
 *
 * Created on November 7, 2019, 6:32 PM
 */


#include "xc.h"
#include "cognition.h"
#include "placeholders.h"
#include "../frimeware/TokenRing.h"
#include "RFID.h"
#include "../mcc_generated_files/system.h"
#include "radio_alpha_trx.h"

/***
 * Cognition section
 */
/* Structure of cognition */
#define cognition_MAX 50

struct {
    uint8_t SIZE;
    uint8_t list[cognition_MAX];
} cognition;

void cognition_init(uint8_t size) {
    if (!(0 < size && size <= cognition_MAX))
        size = cognition_MAX;
    cognition.SIZE = size;
}

/***
 * Set new cognition list from given array
 * @param new_cognition_prog - array containing new cognition program
 * @param size - size of the list
 */
void cognition_set(uint8_t * new_cognition_prog, uint8_t size) {
    cognition_init(size);
    uint8_t i;
    for (i = 0; i < cognition.SIZE; i++)
        cognition.list[i] = new_cognition_prog[i];
}

/***
 * Get element from cognition list
 * @param number - index in cognition list
 * @return value in beheviour list or -1
 */
uint8_t cognition_get(uint8_t number) {
    printf("cognition val %d\n", cognition.list[number]);
    return (0 <= number && number < cognition.SIZE)?cognition.list[number]:(uint8_t) -1;
}

/***
 * DICT dictionary
 */
#define MAX_BIRD 100

struct s_dic {
    uint8_t bird[MAX_BIRD][5];
    uint8_t birds_place[MAX_BIRD];
    int size;
} dictionary;

void DICT_init() {
    dictionary.size = 0;
}

uint8_t DICT_add_bird(const uint8_t * rfid) {
    uint8_t i; 
    if (!(dictionary.size < MAX_BIRD))
        return -1;
    for (i= 0; i < RFID_SIZE; i++) // Copy RFID
        dictionary.bird[dictionary.size][i] = rfid[i];
    dictionary.birds_place[dictionary.size] = 0;
    dictionary.size++;
    return 0;
}


uint8_t DICT_get_bird_cognition_index(const uint8_t * rfid) {
    uint8_t i; 
     for (i = 0; i < dictionary.size; i++) {
         if (RFID_cmp(rfid, dictionary.bird[i])) {
             return dictionary.birds_place[i];
         }
     }
    return DICT_add_bird(rfid);
}

uint8_t DICT_get_bird_current_cognition(const uint8_t * rfid) {
    uint8_t index = DICT_get_bird_cognition_index(rfid);
    return cognition_get(index);
}

void DICT_update_bird(const uint8_t * rfid) {
    uint8_t i; 
    for (i = 0; i < dictionary.size; i++) {
        if (RFID_cmp(rfid, dictionary.bird[i])) {
            dictionary.birds_place[i]++;
            return;
        }
    }
}

/**************** MAIN FUNCTIONNALITY *******************/

void bird_arrived(uint8_t * raw_rfid) {
    uint8_t rfid[5];
    uint8_t i;
    RFID_compress(raw_rfid, rfid);
    i = DICT_get_bird_cognition_index(rfid);
    printf("Pos %d\n", i);
    uint8_t cognition_target_id = cognition_get(i);
    printf("Bird ");
    for (i=0; i < 5; i++) 
        printf("%X", rfid[i]);
    printf(" arrived. Next station = %d, current station = %d\n", cognition_target_id, appData.system_id);
    if (cognition_target_id == (uint8_t) -1)
        return;
    
    if (appData.system_id == cognition_target_id) {
        DOOR_open();
        DICT_update_bird(rfid);
        // <send message>
//        TMR4_Initialize();
//        TMR4_Period16BitSet(0x20);
//        TMR4_Start();
    } else
        DOOR_close();
    
}

void update_bird(uint8_t * rfid) {
    uint8_t cognition_target_id = cognition_get(DICT_get_bird_cognition_index(rfid));
    if (cognition_target_id != (uint8_t) -1)
        DICT_update_bird(rfid);
}