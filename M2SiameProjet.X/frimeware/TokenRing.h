/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_TOKEN_RING_H
#define	XC_TOKEN_RING_H

#include <xc.h> // include processor files - each processor file is guarded.  
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

/*******************************************************************************/
//_______________________________DEFINE________________________________________*/

#define MAX_LEVEL_PRIO 4
#define NB_BEHAVIOR_PER_PRIO 5
#define USE_UART1_SERIAL_INTERFACE
#define SIZE_DATA_TO_SEND 20
#define MAX_RETRANS_TOKEN 10

/*******************************************************************************/
//_______________________________STRUCTURE__ENUM_______________________________*/

typedef enum {
    /* In this state, the application opens the driver */
    APP_STATE_INITIALIZE,

    APP_STATE_IDLE,
    APP_STATE_MSG_RECEIVED,
    APP_STATE_TIMEOUT,
    APP_STATE_ECHO,

    /* Application error state */
    APP_STATE_ERROR

} APP_STATES;

/* Application : 

  Summary:
   

  Description:
    This structure ...

  Remarks:
    ...
 */
typedef enum { // if you want to add a new level, you must be increase MAX_LEVEL_PRIO
    PRIO_EXEPTIONNEL, //00 priorite exeptionnelle 
    PRIO_HIGH, //01
    PRIO_MEDIUM, //10
    PRIO_LOW //11
} PRIORITY;

typedef enum {
    PTR_READ, //00
    PTR_WRITE, //01
    PTR_OVFF //10
} PTR;

/*******************************************************************************/
//_______________________________MAINE_PARAMS__________________________________*/

typedef struct {
    /* System id*/
    uint8_t nbSlave;
    uint8_t system_id;
    uint8_t curentSlave;
    uint8_t broadcast;
    /* Application current state */
    APP_STATES state; /* current state */
    APP_STATES previous_state; /* save previous state */

    /* DateTime structure */
    struct tm current_time;
    volatile uint8_t behavior[MAX_LEVEL_PRIO][NB_BEHAVIOR_PER_PRIO];
    volatile uint8_t ptr[MAX_LEVEL_PRIO][3]; //READ - WRITE - OVFF (overflow)

    uint8_t DATA_TO_SEND[SIZE_DATA_TO_SEND][5];
    uint8_t pRead;
    uint8_t pWrite; // use with data_to_send to have circulary buffer 
    uint8_t nbRetransToken;
    uint8_t nbRetransEcho;
    uint8_t typeMsgSend;
} APP_PARAM;


/*******************************************************************************/
//_______________________________EXTERNE_DECLARATIONS__________________________*/

extern APP_PARAM appData; /* Main application data. */

/*******************************************************************************/


/*******************************************************************************/
//_______________________________MACROS________________________________________*/

#define GETpREAD(prio) appData.ptr[prio][PTR_READ] // recupere le ponteur de lecture
#define GETpWRITE(prio) appData.ptr[prio][PTR_WRITE] // recupere le pointeur d'ecriture
#define GET_OVFF(prio)  appData.ptr[prio][PTR_OVFF] // recipere le pointeur d'overFlow

#define INCpREAD(prio) (appData.ptr[prio][PTR_READ] = (appData.ptr[prio][PTR_READ]+1) % NB_BEHAVIOR_PER_PRIO)
#define INCpWRITE(prio) (appData.ptr[prio][PTR_WRITE] = (appData.ptr[prio][PTR_WRITE]+1) % NB_BEHAVIOR_PER_PRIO)
#define SET_0VFF(prio, set) (appData.ptr[prio][PTR_OVFF] = set)

//_______________________________PROTOTYPES____________________________________*/

/**
 * enregistre le comportement dans le tableau des comportements,
 * en fonction sa priorite
 *  
 * @param behavior : le comportement a traiter 
 * @param prio : la priorite du comportement a traiter 
 * @return :
 *          true  : le comportement est enregistre avec succes 
 *          false : on a ecrase un comportement  
 */
bool APP_StoreBehavior(APP_STATES behavior, PRIORITY prio);

/**
 * 
 * @return : le prochain comportement a adopter 
 */
APP_STATES APP_GetBehavior();


void APP_Init(void);

void APP_Task(void);

void APP_SerialDebugTasks(void);

#endif	/* XC_TOKEN_RING_H */

