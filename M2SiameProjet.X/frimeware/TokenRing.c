/*
 * File:   TokenRing.c
 * Author: mmadi-anzilane
 *
 * Created on 7 novembre 2019, 08:44
 */

/*******************************************************************************/
//_______________________________INCLUDES______________________________________*/

#include "xc.h"
#include "../mcc_generated_files/uart2.h"
#include "TokenRing.h"
#include "radio_alpha_trx.h"


/*******************************************************************************/
//_______________________________EXTERNE_DECLARATIONS__________________________*/

APP_PARAM appData; /* Main application data. */


/*******************************************************************************/
//_______________________________IMPLEMENTATIONS______________________________*//

//______________________________________________________________________________
//______________________________________________________________________________
//________________________________STATE MACHINE FUNCTION________________________

bool APP_StoreBehavior(APP_STATES state, PRIORITY prio) {
    appData.behavior[prio][GETpWRITE(prio)] = state; // on ecrit le comportement 
    INCpWRITE(prio);
    if (GETpREAD(prio) == GETpWRITE(prio)) { // Je viens d'ecraser un comportement 
        //        SET_0VFF(prio, 1); // overflow
        INCpREAD(prio);
    }

    return true; // 
}

APP_STATES APP_GetBehavior() {
    // on cmmence par chercher un comportement de prio eleve
    APP_STATES state = APP_STATE_IDLE;
    // l'ordre des condition est important car on respect la priorite
    if (GETpREAD(PRIO_EXEPTIONNEL) != GETpWRITE(PRIO_EXEPTIONNEL)) {
        // on ne peut avoir l'egalite et a voir un comportement present 
        state = appData.behavior[PRIO_EXEPTIONNEL][GETpREAD(PRIO_EXEPTIONNEL)];
        INCpREAD(PRIO_EXEPTIONNEL);
    } else if (GETpREAD(PRIO_HIGH) != GETpWRITE(PRIO_HIGH)) {
        // on ne peut avoir l'egalite et a voir un comportement present 
        state = appData.behavior[PRIO_HIGH][GETpREAD(PRIO_HIGH)];
        INCpREAD(PRIO_HIGH);
    } else if (GETpREAD(PRIO_MEDIUM) != GETpWRITE(PRIO_MEDIUM)) {
        state = appData.behavior[PRIO_MEDIUM][GETpREAD(PRIO_MEDIUM)];
        INCpREAD(PRIO_MEDIUM);
    } else if (GETpREAD(PRIO_LOW) != GETpWRITE(PRIO_LOW)) {
        state = appData.behavior[PRIO_LOW][GETpREAD(PRIO_LOW)];
        INCpREAD(PRIO_LOW);
    }
    return state;
}

/**
 * 
 * @param nbTrans
 */
void APP_TransmitToken(int8_t nbTrans) {
    //transmettre les jeton 
    radioAlphaTRX_SendMsgRF(appData.system_id,
            (appData.system_id + 1) % appData.nbSlave,
            TOKEN, nbTrans,
            0, (uint8_t*) "", 0); // change 5 to generique value
    // start tempo
    TMR_SetWaitRqstTimeout(TIME_OUT_WAIT_TOKEN_REQUEST);
    appData.typeMsgSend = TOKEN;
}

/**
 * 
 * @param idSlave
 */
void APP_PullingEcho(uint8_t idSlave) {
    //transmettre les jeton 
    radioAlphaTRX_SendMsgRF(appData.system_id,
            idSlave,
            ECHO, 0,
            0, (uint8_t*) "", 0); // change 5 to generique value
    // start tempo
    TMR_SetWaitRqstTimeout(TIME_OUT_WAIT_TOKEN_REQUEST);
    appData.typeMsgSend = ECHO;
}

/**
 * main task
 * 
 */
void APP_Task(void) {

    appData.state = APP_GetBehavior();

    switch (appData.state) {
        case APP_STATE_INITIALIZE:
            if (appData.state != appData.previous_state) {
                appData.previous_state = appData.state;
#if defined( USE_UART1_SERIAL_INTERFACE )
                printf(">APP_STATE_INITIALIZE\n");
#endif
            }

            // may be we can change that 
            radioAlphaTRX_Init();
            radioAlphaTRX_ReceivedMode();
            MSG_RECEIVE = false;

            if (1 == appData.system_id) {
                APP_StoreBehavior(APP_STATE_ECHO, PRIO_HIGH);
            }

            break;
        case APP_STATE_ECHO:
            if (appData.state != appData.previous_state) {
                appData.previous_state = appData.state;
#if defined( USE_UART1_SERIAL_INTERFACE )
                printf(">APP_STATE_ECHO\n");
#endif
            }
            APP_PullingEcho(appData.curentSlave);
            break;
        case APP_STATE_IDLE:
            if (appData.state != appData.previous_state) {
                appData.previous_state = appData.state;
#if defined( USE_UART1_SERIAL_INTERFACE )
                printf(">APP_STATE_IDLE\n");
#endif
            }
            APP_SerialDebugTasks();
            break;
        case APP_STATE_MSG_RECEIVED:
        {
            if (appData.state != appData.previous_state) {
                appData.previous_state = appData.state;
#if defined( USE_UART1_SERIAL_INTERFACE )
                printf(">APP_STATE_MSG_RECEIVED\n");
#endif
            }

            /**
             * Action in this state 
             *  stop timer 
             *  extract msg type
             *      execute traitment for token msg 
             */

            // je pense qu'il serait plus juducieux de transmettre des acks concerant 
            // la reception du jeton 
            TMR_SetWaitRqstTimeout(-1); // stop Tempo

            Frame msgReceived = radioAlphaTRX_GetFrame();
            switch (msgReceived.Champ.typeMsg) {
                case TOKEN:
                {
                    // token received
                    if (msgReceived.Champ.idMsg == appData.system_id) {
                        int8_t i = 0;
                        while (appData.pRead != appData.pWrite) {
                            // send Data 
                            radioAlphaTRX_SendMsgRF(appData.system_id,
                                    appData.broadcast,
                                    DATA, appData.pRead,
                                    i++, appData.DATA_TO_SEND[appData.pRead], 5); // change 5 to generique value
                            //on increment 
                            appData.pRead = (appData.pRead + 1) % SIZE_DATA_TO_SEND;
                            TMR_Delay(20); // wait 20 ms to send an others msg
                        }
                        //transmettre le jeton 
                        APP_TransmitToken(0);
                    } else {
                        appData.nbRetransToken = MAX_RETRANS_TOKEN;
                    }
                }
                    break;
                case DATA: // RFID 

                    break;
                case ACK:
                    if (ECHO == appData.typeMsgSend) {
                        appData.curentSlave++;
                        appData.typeMsgSend = NOTHING;
                        if (appData.curentSlave < appData.nbSlave) {
                            APP_StoreBehavior(APP_STATE_ECHO, PRIO_HIGH);
                        }
                    }
                    break;
                default:
#if defined (USE_UART1_SERIAL_INTERFACE) 
                    printf("default state %d type msg received \n",
                            msgReceived.Champ.typeMsg);
#endif
            }

        }
            break;
        case APP_STATE_TIMEOUT:
        {
            if (appData.state != appData.previous_state) {
                appData.previous_state = appData.state;
#if defined( USE_UART1_SERIAL_INTERFACE )
                printf(">APP_STATE_TIMEOUT\n");
#endif
            }
            if (TOKEN == appData.typeMsgSend) {
                // if we enter here, i.e TOKEN not Transmit
                appData.nbRetransToken--;
                if (appData.nbRetransToken > 0) {
                    //transmettre le jeton 
                    APP_TransmitToken(appData.nbRetransToken - MAX_RETRANS_TOKEN);
                } else {
#if defined (USE_UART1_SERIAL_INTERFACE) 
                    printf("Le Token est perdu ==> election\n");
#endif
                }
            } else if (ECHO == appData.typeMsgSend) {
                appData.nbRetransEcho--;
                if (appData.nbRetransEcho > 0) {
                    APP_PullingEcho(appData.curentSlave);
                } else {
#if defined (USE_UART1_SERIAL_INTERFACE) 
                    printf("Le cecle n'est pas complet, un slave manque\n");
#endif
                }
            }

        }
            break;
        default:
            if (appData.state != appData.previous_state) {
                appData.previous_state = appData.state;
#if defined ( USE_UART1_SERIAL_INTERFACE )
                printf(">Default State\n");
#endif
            }

    }
}

void APP_Init(void) {
    int8_t i, j;
    /* APP state task */
    for (i = 0; i < NB_BEHAVIOR_PER_PRIO; i++) {
        for (j = 0; j < 3; j++)
            appData.ptr[i][j] = 0;
    }


    APP_StoreBehavior(APP_STATE_INITIALIZE, PRIO_HIGH);
    //    appData.state = MASTER_APP_STATE_INITIALIZE;
    appData.previous_state = APP_STATE_ERROR;

    appData.system_id = 1;
    appData.curentSlave = 2;
    appData.broadcast = 255;
    appData.nbSlave = 2;
    appData.nbRetransToken = MAX_RETRANS_TOKEN;
    appData.typeMsgSend = NOTHING;
}


//__________________________DEBUG_______________________________________________

void display_STATUS_register_from_RF_module(void) {

    /* Read RF module STATUS register */
    RF_StatusRead.Val = radioAlphaTRX_Command(STATUS_READ_CMD);
    printf("Read RF STATUS register: 0x%04X\n", RF_StatusRead.Val); // 4.1. ?criture format?e de donn?es --> https://www.ltam.lu/cours-c/prg-c42.htm
    printf("Bit [ 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 ]\n");
    printf("    [  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u |  %u ]\n", \
                        RF_StatusRead.bits.b15_RGIT_FFIT, \
                        RF_StatusRead.bits.b14_POR, \
                        RF_StatusRead.bits.b13_RGUR_FFOV, \
                        RF_StatusRead.bits.b12_WKUP, \
                        RF_StatusRead.bits.b11_EXT, \
                        RF_StatusRead.bits.b10_LBD, \
                        RF_StatusRead.bits.b9_FFEM, \
                        RF_StatusRead.bits.b8_ATSS, \
                        RF_StatusRead.bits.b7_ATS_RSSI, \
                        RF_StatusRead.bits.b6_DQD, \
                        RF_StatusRead.bits.b5_CRL, \
                        RF_StatusRead.bits.b4_ATGL, \
                        RF_StatusRead.bits.b3_OFFS_Sign, \
                        RF_StatusRead.bits.b2_OFFS_b2, \
                        RF_StatusRead.bits.b1_OFFS_b1, \
                        RF_StatusRead.bits.b0_OFFS_b0 \
                        );
    printf("     FFIT| POR|FFOV|WKUP| EXT| LBD|FFEM|RSSI| DQD| CRL|ATGL|OFFS|OFFS|OFFS|OFFS|OFFS|\n"); // transceiver in receive (RX) mode, bit 'er' is set
    printf("     RGIT      RGUR                      ATS                 <6>  <3>  <2>  <1>  <0>\n"); // bit 'er' is cleared

}


/* Current date to a C string (page 244)) */
const uint8_t BUILD_DATE[] = {__DATE__};
const uint8_t BUILD_TIME[] = {__TIME__};

void displayKeyMapping(void) {
    /* Interface firmware terminal (Debug) */
    printf("\tKey mapping:\n");
    printf("\t a or A: Not Use\n");
}

void APP_SerialDebugTasks(void) {
    //    int i;
    //    bool flag;

    if (UART2_TRANSFER_STATUS_RX_DATA_PRESENT & UART2_TransferStatusGet()) {
        /* Re-enable 60s max dely of inactivity to prevent system reset */
        TMR_SetTimeout(60000);

        /* If at least one byte of data has been received. */
        uint8_t data_from_uart2 = UART2_Read();

        switch (data_from_uart2) {
            case '?':
                displayKeyMapping();
                break;

            case 'a':
            case 'A':
            {
                uint8_t user_choice;

                TMR_SetTimeout(MAX_READ_FROM_UART_DELAY);

                while (false == (UART2_TRANSFER_STATUS_RX_DATA_PRESENT & UART2_TransferStatusGet()) && 0 > TMR_GetTimeout());
                if (0 == TMR_GetTimeout()) {
                    printf("\n\tToo slow entering value => command aborted\n");
                    break;
                }
                user_choice = UART2_Read();

                switch (user_choice) {
                    case 'b':
                    case 'B':
                    {
#if defined (USE_UART1_SERIAL_INTERFACE) 
                        printf("Test \n");
#endif
                    }
                        break;
                }
                break;
            }
            case 'r':
            case 'R':
                display_STATUS_register_from_RF_module();
                break;
            default:
                putchar(9);
                putchar(data_from_uart2); /* echo RX data if doesn't match */
                putchar(' ');
                putchar('(');
                putchar('d');
                putchar(')');
                putchar(13);
                putchar(10);
                break;
        }
    } /* end of if ( UART1_TRANSFER_STATUS_RX_DATA_PRESENT & UART1_TransferStatusGet( ) ) */
}
